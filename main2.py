# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_item(item_id: int):
#     return {"item_id": item_id}

########################################################3

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/users/me")
# async def read_user_me():
#     return {"user_id": "the current user"}
#
#
# @app.get("/users/{user_id}")
# async def read_user(user_id: str):
#     return {"user_id": user_id}

###################################################33

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/users")
# async def read_users():
#     return ["Rick", "Morty"]
#
#
# @app.get("/users")
# async def read_users2():
#     return ["Bean", "Elfo"]

###################################################

# from enum import Enum
#
# from fastapi import FastAPI
#
#
# class ModelName(str, Enum):
#     alexnet = "alexnet"
#     resnet = "resnet"
#     lenet = "lenet"
#
#
# app = FastAPI()
#
#
# @app.get("/models/{model_name}")
# async def get_model(model_name: ModelName):
#     if model_name == ModelName.alexnet:
#         return {"model_name": model_name, "message": "Deep Learning FTW!"}
#
#     if model_name.value == "lenet":
#         return {"model_name": model_name, "message": "LeCNN all the images"}
#
#     return {"model_name": model_name, "message": "Have some residuals"}

#################################################3

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/files/{file_path:path}")
# async def read_file(file_path: str):
#     return {"file_path": file_path}

#############################################################3

# from fastapi import FastAPI
#
# app = FastAPI()
#
# fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]
#
#
# @app.get("/items/")
# async def read_item(skip: int = 0, limit: int = 10):
#     return fake_items_db[skip : skip + limit]


############################################################
# from __future__ import annotations
#
# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_item(item_id: str, q: str or None = None):
#     if q:
#         return {"item_id": item_id, "q": q}
#     return {"item_id": item_id}


#######################################################


# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_item(item_id: str, q: str or None = None, short: bool = False):
#     item = {"item_id": item_id}
#     if q:
#         item.update({"q": q})
#     if not short:
#         item.update(
#             {"description": "This is an amazing item that has a long description"}
#         )
#     return item

#####################################################

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/users/{user_id}/items/{item_id}")
# async def read_user_item(
#     user_id: int, item_id: str, q: str or None = None, short: bool = False
# ):
#     item = {"item_id": item_id, "owner_id": user_id}
#     if q:
#         item.update({"q": q})
#     if not short:
#         item.update(
#             {"description": "This is an amazing item that has a long description"}
#         )
#     return item


#########################################################

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/users/{user_id}/{q}")
# async def read_user_item(user_id: int, item_id: str or None=None, q: str or None = None, short: bool = False):
#     item = {"item_id": item_id, "owner_id": user_id}
#     if q:
#         item.update({"q": q})
#     if not short:
#         item.update(
#             {"description": "This is an amazing item that has a long description"}
#         )
#     return item

############################################################3

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_user_item(item_id: str, needy: str or None = None):
#     item = {"item_id": item_id, "needy": needy }
#     return item

################################################################

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_user_item(
#     item_id: str, needy: str, skip: int = 0, limit: int or None = None
# ):
#     item = {"item_id": item_id, "needy": needy, "skip": skip, "limit": limit}
#     return item

###############################################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# app = FastAPI()
#
#
# @app.post("/items/")
# async def create_item(item: Item):
#     return item

#################################################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
#
# class Capital(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# app = FastAPI()
#
#
# @app.post("/items/")
# async def create_item(item: Capital):
#     return item

##################################################################3

# from fastapi import FastAPI
# from pydantic import BaseModel
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# app = FastAPI()
#
#
# @app.post("/items/")
# async def create_item(item: Item):
#     item_dict = item.dict()
#     if item.tax:
#         price_with_tax = item.price + item.tax
#         item_dict.update({"price_with_tax": price_with_tax})
#     return item_dict

#############################################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# app = FastAPI()
#
#
# @app.put("/items/{item_id}")
# async def create_item(item_id: int, item: Item):
#     return {"item_id": item_id, **item.dict()}

##############################################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# app = FastAPI()
#
#
# @app.put("/items/{item_id}")
# async def create_item(item_id: int, item: Item, q: str or None = None):
#     result = {"item_id": item_id, **item.dict()}
#     if q:
#         result.update({"q": q})
#     return result

############################################################

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str or None = None):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

##############################################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
# '''
# q: str | None = Query(default=None)
#
# ...makes the parameter optional, the same as:
#
#
# q: str | None = None
# '''
#
#
# @app.get("/items/")
# async def read_items(q: str or None = Query(default=None, max_length=50)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results


##########################################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str or None = Query(default=None, min_length=3, max_length=50)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

###########################################################3

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
# '''
# This specific regular expression checks that the received parameter value:
#
# ^: starts with the following characters, doesn't have characters before.
# fixedquery: has the exact value fixedquery.
# $: ends there, doesn't have any more characters after fixedquery.
# '''
#
#
# @app.get("/items/")
# async def read_items(q: str or None = Query(default=None, min_length=3, max_length=50, regex="^fixedquery$")):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

###########################################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str = Query(default="fixedquery", min_length=3)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results
#

###########################################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str = Query(min_length=3)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

#################################################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str = Query(default=..., min_length=3)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

#################################################################3

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str or None = Query(default=..., min_length=3)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

#############################################################33

# from fastapi import FastAPI, Query
# from pydantic import Required
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str = Query(default=Required, min_length=3)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

###################################################################3

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: list[str] or None = Query(default=None)):
#     query_items = {"q": q}
#     return query_items

############################################################3

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: list[str] = Query(default=["foo", "bar"])):
#     query_items = {"q": q}
#     return query_items

####################################################33

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: list[str] = Query(default=[""])):
#     query_items = {"q": q}
#     return query_items

######################################################33

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: list = Query(default=[])):
#     query_items = {"q": q}
#     return query_items

##################################################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(
#     q: str or None = Query(default=None, title="Query string", min_length=3)
# ):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

###########################################################3#

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(
#     q: str
#     or None = Query(
#         default=None,
#         title="Queryyyyyyyyyyyyyyyyyyyyyyyyy string",
#         description="Query string for the items to search in the database that have a good matchhhhhhhhhhhh",
#         min_length=3,
#     )
# ):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

######################################################33

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str or None = Query(default=None, alias="itemmmmm-query")):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

###############################################################3

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(q: str or None = Query(
#         default=None,
#         alias="item-query",
#         title="Query string",
#         description="Query string for the items to search in the database that have a good match",
#         min_length=3,
#         max_length=50,
#         regex="^fixedquery$",
#         deprecated=True,
#     )
# ):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

###############################################

# from fastapi import FastAPI, Query
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(
#     hidden_query: str or None = Query(default=None, include_in_schema=False)
# ):
#     if hidden_query:
#         return {"hidden_query": hidden_query}
#     else:
#         return {"hidden_query": "Not found"}

#########################################################

# from fastapi import FastAPI, Path, Query
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(
#     item_id: int = Path(title="The ID of the item to get"),
#     q: str or None = Query(default=None, alias="item-query"),
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

#############################################################3

# from fastapi import FastAPI, Path, Query
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(
#     item_id: int = Path(title="The ID of the item to get"),
#     q: str or None = Query(default=None, alias="item-query"),
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

#####################################################

# from fastapi import FastAPI, Path
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(q: str, item_id: int = Path(title="The ID of the item to get")):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

#############################################################

# from fastapi import FastAPI, Path
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(*, item_id: int = Path(title="The ID of the item to get"), q: str):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

#########################################################

# from fastapi import FastAPI, Path
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(
#     *, item_id: int = Path(title="The ID of the item to get", ge=1), q: str
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

###############################################33

# from fastapi import FastAPI, Path
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(
#     *,
#     item_id: int = Path(title="The ID of the item to get", gt=0, le=1000),
#     q: str,
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

######################################################3

# from fastapi import FastAPI, Path, Query
#
# app = FastAPI()
#
#
# @app.get("/items/{item_id}")
# async def read_items(
#     *,
#     item_id: int = Path(title="The ID of the item to get", ge=0, le=1000),
#     q: str,
#     size: float = Query(gt=0, lt=10.5)
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results

###################################################3

# from fastapi import FastAPI, Path
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(
#     *,
#     item_id: int = Path(title="The ID of the item to get", ge=0, le=1000),
#     q: str or None = None,
#     item: Item or None = None,
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     if item:
#         results.update({"item": item})
#     return results

##################################################################33

# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# class User(BaseModel):
#     username: str
#     full_name: str or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item, user: User):
#     results = {"item_id": item_id, "item": item, "user": user}
#     return results

####################################################################33

# from fastapi import Body, FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# class User(BaseModel):
#     username: str
#     full_name: str or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item, user: User, importance: int = Body()):
#     results = {"item_id": item_id, "item": item, "user": user, "importance": importance}
#     return results

#############################################################3

# from fastapi import Body, FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# class User(BaseModel):
#     username: str
#     full_name: str or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(
#     *,
#     item_id: int,
#     item: Item,
#     user: User,
#     importance: int = Body(gt=0),
#     q: str or None = None
# ):
#     results = {"item_id": item_id, "item": item, "user": user, "importance": importance}
#     if q:
#         results.update({"q": q})
#     return results

##############################################################3

# from fastapi import Body, FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item = Body(embed=True)):
#     results = {"item_id": item_id, "item": item}
#     return results
#
# '''
#
# In this case FastAPI will expect a body like:
#
#
# {
#     "item": {
#         "name": "Foo",
#         "description": "The pretender",
#         "price": 42.0,
#         "tax": 3.2
#     }
# }
# instead of:
#
#
# {
#     "name": "Foo",
#     "description": "The pretender",
#     "price": 42.0,
#     "tax": 3.2
# }
#
# '''

##########################################################3

# from fastapi import Body, FastAPI
# from pydantic import BaseModel, Field
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = Field(
#         default=None, title="The description of the item", max_length=300
#     )
#     price: float = Field(gt=0, description="The price must be greater than zero")
#     tax: float or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item = Body(embed=True)):
#     results = {"item_id": item_id, "item": item}
#     return results

##########################################################

# from fastapi import Body, FastAPI
# from pydantic import BaseModel, Field
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = Field(
#         default=None, title="The description of the item", max_length=300
#     )
#     price: float = Field(gt=0, description="The price must be greater than zero")
#     tax: float or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item = Body(embed=True)):
#     results = {"item_id": item_id, "item": item}
#     return results

####################################################3

# from typing import List, Union
#
# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: Union[str, None] = None
#     price: float
#     tax: Union[float, None] = None
#     tags: List[str] = []
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results

###########################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#     tags: set[str] = set()
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results

###############################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Website(BaseModel):
#     url: str
#     name: str
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#     tags: set[str] = set()
#     image: Website or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results

#########################################33

# from fastapi import FastAPI
# from pydantic import BaseModel, HttpUrl
#
# app = FastAPI()
#
#
# class Image(BaseModel):
#     url: HttpUrl
#     name: str
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#     tags: set[str] = set()
#     image: Image or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results

#####################################################33

# from fastapi import FastAPI
# from pydantic import BaseModel, HttpUrl
#
# app = FastAPI()
#
#
# class Image(BaseModel):
#     url: HttpUrl
#     name: str
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#     tags: set[str] = set()
#     images: list[Image] or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results

##################################3

# from fastapi import FastAPI
# from pydantic import BaseModel, HttpUrl
#
# app = FastAPI()
#
#
# class Image(BaseModel):
#     url: HttpUrl
#     name: str
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#     tags: set[str] = set()
#     images: list[Image] or None = None
#
#
# class Offer(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     items: list[Item]
#
#
# @app.post("/offers/")
# async def create_offer(offer: Offer):
#     return offer
#

######################################################


# from fastapi import FastAPI
# from pydantic import BaseModel, HttpUrl
#
# app = FastAPI()
#
#
# class Image(BaseModel):
#     image_url: HttpUrl
#     image_name: str
#
#
# class Item(BaseModel):
#     item_name: str
#     item_description: str or None = None
#     item_price: float
#     item_tax: float or None = None
#     item_tags: set[str] = set()
#     item_images: list[Image] or None = None
#
#
# class Offer(BaseModel):
#     offer_name: str
#     offer_description: str or None = None
#     offer_price: float
#     offer_items: list[Item]
#
#
# @app.post("/offers/")
# async def create_offer(offer: Offer):
#     return offer

#####################################################################3

# from fastapi import FastAPI
# from pydantic import BaseModel, HttpUrl
#
# app = FastAPI()
#
#
# class Image(BaseModel):
#     url: HttpUrl
#     name: str
#
#
# @app.post("/images/multiple/")
# async def create_multiple_images(images: list[Image]):
#     return images

###########################################################################

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.post("/index-weights/")
# async def create_index_weights(weights: dict[int, float]):
#     return weights

#################################################################3

# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#     class Config:
#         schema_extra = {
#             "example": {
#                 "name": "Foo",
#                 "description": "A very nice Item",
#                 "price": 35.4,
#                 "tax": 3.2,
#             }
#         }
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results
#
#

#############################################################3

# from fastapi import FastAPI
# from pydantic import BaseModel, Field
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str = Field(example="Foo")
#     description: str or None = Field(default=None, example="A very nice Item")
#     price: float = Field(example=35.4)
#     tax: float or None = Field(default=None, example=3.2)
#
#
# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results

##########################################################

#
# from fastapi import Body, FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(
#     item_id: int,
#     item: Item = Body(
#         example={
#             "name": "Foo",
#             "description": "A very nice Item",
#             "price": 35.4,
#             "tax": 3.2,
#         },
#     ),
# ):
#     results = {"item_id": item_id, "item": item}
#     return results

########################################################################3

# from fastapi import Body, FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#
#
# @app.put("/items/{item_id}")
# async def update_item(
#     *,
#     item_id: int,
#     item: Item = Body(
#         examples={
#             "normal": {
#                 "summary": "A normal example",
#                 "description": "A **normal** item works correctly.",
#                 "value": {
#                     "name": "Foo",
#                     "description": "A very nice Item",
#                     "price": 35.4,
#                     "tax": 3.2,
#                 },
#             },
#             "converted": {
#                 "summary": "An example with converted data",
#                 "description": "FastAPI can convert price `strings` to actual `numbers` automatically",
#                 "value": {
#                     "name": "Bar",
#                     "price": "35.4",
#                 },
#             },
#             "invalid": {
#                 "summary": "Invalid data is rejected with an error",
#                 "value": {
#                     "name": "Baz",
#                     "price": "thirty five point four",
#                 },
#             },
#         },
#     ),
# ):
#     results = {"item_id": item_id, "item": item}
#     return results

#####################################################################33

# from datetime import datetime, time, timedelta
# from uuid import UUID
#
# from fastapi import Body, FastAPI
#
# app = FastAPI()
#
#
# @app.put("/items/{item_id}")
# async def read_items(
#     item_id: UUID,
#     start_datetime: datetime or None = Body(default=None),
#     end_datetime: datetime or None = Body(default=None),
#     repeat_at: time or None = Body(default=None),
#     process_after: timedelta or None = Body(default=None),
# ):
#     start_process = start_datetime + process_after
#     duration = end_datetime - start_process
#     return {
#         "item_id": item_id,
#         "start_datetime": start_datetime,
#         "end_datetime": end_datetime,
#         "repeat_at": repeat_at,
#         "process_after": process_after,
#         "start_process": start_process,
#         "duration": duration,
#     }


############################################################

# from fastapi import Cookie, FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(ads_id: str or None = Cookie(default=None)):
#     return {"ads_id": ads_id}

############################################################

# from fastapi import Cookie, FastAPI
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(ads_id: str or None = Cookie(default=None)):
#     return {"ads_id": ads_id}

###############################################################

# from fastapi import FastAPI, Header
#
# app = FastAPI()
#
#
# @app.get("/items/")
# async def read_items(user_agent: str or None = Header(default=None)):
#     return {"User-Agent": user_agent}

###################################################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str or None = None
#     price: float
#     tax: float or None = None
#     tags: list[str] = []
#
#
# @app.post("/items/", response_model=Item)
# async def create_item(item: Item):
#     return item

#####################################################################

# from fastapi import FastAPI
# from pydantic import BaseModel, EmailStr
#
# app = FastAPI()
#
#
# class UserIn(BaseModel):
#     username: str
#     password: str
#     email: EmailStr
#     full_name: str or None = None
#
#
# # Don't do this in production!
# @app.post("/user/", response_model=UserIn)
# async def create_user(user: UserIn):
#     return user

#######################################################################

# from fastapi import FastAPI
# from pydantic import BaseModel, EmailStr
#
# app = FastAPI()
#
#
# class UserIn(BaseModel):
#     username: str
#     password: str or None=None
#     email: EmailStr
#     full_name: str or None = None
#
#
# class UserOut(BaseModel):
#     username: str
#     email: EmailStr
#     full_name: str or None = None
#
#
# @app.post("/user/", response_model=UserOut)
# async def create_user(user: UserIn):
#     return user

#####################################################################33

'''    TODO API    '''
#
#
# from fastapi import FastAPI, HTTPException
# from pydantic import BaseModel
# from typing import Optional, List
#
# class Todo(BaseModel):
#
#     name: str
#     due_date: str
#     description: str
#
# app = FastAPI(title="TODO API")
#
# store_todo = []
#
# @app.get('/')
# async def home():
#     return {"Hello":"World"}
#
# @app.post('/todo/')
# async def create_todo(todo:Todo):
#     store_todo.append(todo)
#     return todo
#
# @app.get('/todo', response_model=List[Todo])
# async def get_all_todos():
#     return store_todo
#
# @app.get('/todo/{id}')
# async def get_todo(id:int):
#
#     try:
#         return store_todo[id]
#     except:
#         HTTPException(status_code=404, detail="Todo Not Found")
#
# @app.put('/linux{id}')
# async def update_todo(id: int, todo:Todo):
#
#     try:
#         store_todo[id] = todo
#         return store_todo[id]
#
#     except:
#         raise HTTPException(status_code=404, detail="Todo Not Found")
#
# @app.delete('/todo/{id}')
# async def delete_linux(id:int):
#     try:
#         obj = store_todo[id]
#         store_todo.pop(id)
#         return obj
#
#     except:
#         raise HTTPException(status_code=404, detail="Todo Not Found")

##################################################################################3

# from fastapi import FastAPI
# from pydantic import BaseModel, EmailStr
#
# app = FastAPI()
#
#
# class UserIn(BaseModel):
#     username: str
#     password: str
#     email: EmailStr
#     full_name: str or None = None
#
#
# class UserOut(BaseModel):
#     username: str
#     email: EmailStr
#     full_name: str or None = None
#
#
# class UserInDB(BaseModel):
#     username: str
#     hashed_password: str
#     email: EmailStr
#     full_name: str or None = None
#
#
# def fake_password_hasher(raw_password: str):
#     return "supersecret" + raw_password
#
#
# def fake_save_user(user_in: UserIn):
#     hashed_password = fake_password_hasher(user_in.password)
#     user_in_db = UserInDB(**user_in.dict(), hashed_password=hashed_password)
#     print("User saved! ..not really")
#     return user_in_db
#
#
# @app.post("/user/", response_model=UserOut)
# async def create_user(user_in: UserIn):
#     user_saved = fake_save_user(user_in)
#     return user_saved

####################################################################################3

# from fastapi import FastAPI
# from pydantic import BaseModel, EmailStr
#
# app = FastAPI()
#
#
# class UserBase(BaseModel):
#     username: str
#     email: EmailStr
#     full_name: str or None = None
#
#
# class UserIn(UserBase):
#     password: str
#
#
# class UserOut(UserBase):
#     print("userout!!!!!!!!!!!!!!!")
#
#
# class UserInDB(UserBase):
#     hashed_password: str
#
#
# def fake_password_hasher(raw_password: str):
#     return "supersecret" + raw_password
#
#
# def fake_save_user(user_in: UserIn):
#     hashed_password = fake_password_hasher(user_in.password)
#     user_in_db = UserInDB(**user_in.dict(), hashed_password=hashed_password)
#     print("User saved! ..not really")
#     return user_in_db
#
#
# @app.post("/user/", response_model=UserOut)
# async def create_user(user_in: UserIn):
#     user_saved = fake_save_user(user_in)
#     return user_saved

#############################################################################

# from typing import Union
#
# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class BaseItem(BaseModel):
#     description: str
#     type: str
#
#
# class CarItem(BaseItem):
#     type = "car"
#
#
# class PlaneItem(BaseItem):
#     type = "plane"
#     size: int
#
#
# items = {
#     "item1": {"description": "All my friends drive a low rider", "type": "car"},
#     "item2": {
#         "description": "Music is my aeroplane, it's my aeroplane",
#         "type": "plane",
#         "size": 5,
#     },
# }
#
#
# @app.get("/items/{item_id}", response_model=Union[PlaneItem, CarItem])
# async def read_item(item_id: str):
#     return items[item_id]

##########################################################################

# from fastapi import FastAPI
# from pydantic import BaseModel
#
# app = FastAPI()
#
#
# class Item(BaseModel):
#     name: str
#     description: str
#
#
# items = [
#     {"name": "Foo", "description": "There comes my hero"},
#     {"name": "Red", "description": "It's my aeroplane"},
# ]
#
#
# @app.get("/items/", response_model=list[Item])
# async def read_items():
#     return items

####################################################################

# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/keyword-weights/", response_model=dict[str, float])
# async def read_keyword_weights():
#     return {"foo": 2.3, "bar": 3.4}


from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_item(item_id: str, q: str or None = None, short: bool = False):
    item = {"item_id": item_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item